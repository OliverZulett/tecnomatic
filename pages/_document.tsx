import React from "react";
import Document, { Html, Head, Main, NextScript } from 'next/document'

export default function MyDocument() {
  return (
    <Html data-theme="emerald">
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
