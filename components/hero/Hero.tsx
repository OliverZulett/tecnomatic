import React, { useCallback, useState } from "react";
import styled from "styled-components";
import ProductSlider from "./ProductSlider";


const HeroStyled = styled.div`
  min-height: 800px;
  @media only screen and (min-width: 768px) {
    min-height: 550px;
  }
`;

export default function Hero() {
  const mockProducts = [
    {
      id: "1",
      title: "KIT AUTOMATIZACIÓN PORTON CORREDIZO",
      subTitle: "MOTOR SEMI-INDUSTRIAL CORREDIZO",
      productImageUrl:
        "https://tecnomatic.com.bo/wp-content/uploads/2020/05/motor-corredizo.jpg",
      providerImageUrl:
        "https://tecnomatic.com.bo/wp-content/uploads/2020/05/garen2-117x70.png",
    },
    {
      id: "2",
      title: "CONTROL ACCESO & ASISTENCIA HUELLA K40/ID",
      subTitle: null,
      productImageUrl:
        "https://tecnomatic.com.bo/wp-content/uploads/2020/05/K40.png",
      providerImageUrl:
        "https://tecnomatic.com.bo/wp-content/uploads/2020/05/zkteco.png",
    },
    {
      id: "3",
      title: "CAMARA DOMO 4EN1 HAC-HDW1200MN-0280B-S3A",
      subTitle: null,
      productImageUrl:
        "https://tecnomatic.com.bo/wp-content/uploads/2020/05/hdw1200mn.jpg",
      providerImageUrl:
        "https://tecnomatic.com.bo/wp-content/uploads/2020/05/dahua.png",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur fugit expedita quo nulla velit alias quidem quaerat inventore nisi error.",
    },
  ];


  const [productSelected, setProductSelected] = useState(0);
  const [product, setProduct] = useState(mockProducts[productSelected]);
  const {
    id,
    title,
    subTitle,
    productImageUrl,
    providerImageUrl,
    description,
  } = product;

  const changeProductSlide = useCallback(
    (modifier: number) => {
      const productsLength = mockProducts.length - 1;
      if (modifier >= 0 && modifier < productsLength) {
        setProductSelected(modifier + 1);
      } else if (modifier < 0 || modifier === productsLength) {
        setProductSelected(0);
      }
      setProduct(mockProducts[productSelected]);
    },
    [productSelected]
  );

  return (
    // <HeroStyled className="w-full carousel h-screen">
    <HeroStyled >
      {
        <div
          id={`slide${id}`}
        >
          {/* className="flex justify-center relative w-full h-full  carousel-item" */}
          <ProductSlider
            key={id}
            id={id}
            title={title}
            subTitle={subTitle || ""}
            productImageUrl={productImageUrl}
            providerImageUrl={providerImageUrl}
            description={description || ""}
          />
          <div className="absolute flex justify-between transform -translate-y-40 left-5 right-5 top-1/2">
            <button
              className="btn btn-outline btn-circle btn-sm btn-primary"
              onClick={(e) => {
                changeProductSlide(productSelected);
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M15 19l-7-7 7-7"
                />
              </svg>
            </button>
            <button
              className="btn btn-outline btn-circle btn-sm btn-primary"
              onClick={(e) => {
                changeProductSlide(productSelected);
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M9 5l7 7-7 7"
                />
              </svg>
            </button>
          </div>
        </div>
      }
    </HeroStyled>
  );
}
