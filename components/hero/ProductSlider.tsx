import React from "react";
import { Animated } from "react-animated-css";
import Image from 'next/image';
import styled from "styled-components";

interface ProductSliderProps {
  id: string;
  title: string;
  subTitle: string;
  productImageUrl: string;
  providerImageUrl: string;
  description: string;
}

const SlideStyled = styled.div`
  min-height: 800px;
  @media only screen and (min-width: 768px) {
    min-height: 550px;
  }
`;

const ProductSlider: React.FunctionComponent<ProductSliderProps> = ({
  id,
  title,
  subTitle,
  productImageUrl,
  providerImageUrl,
  description,
}) => {
  if (!id || (!title && !productImageUrl)) return null;
  return (
    <SlideStyled className="p-5 flex flex-col md:flex-row md:items-center">
      <Animated
        animationIn="bounceInLeft"
        animationOut="bounceInRight"
        animationInDuration={1400}
        animationOutDuration={1400}
        isVisible={true}
        className="h-96 md:h-full md:w-6/12 lg:w-5/12 flex justify-center md:justify-end items-center md:mr-10"
      >
        <img
          src={productImageUrl}
          className="w-80 rounded-lg shadow-2xl"
          alt="Picture of the author"
        />
      </Animated>

      <Animated
        animationIn="fadeIn"
        animationOut="fadeOut"
        animationInDuration={2000}
        animationOutDuration={2000}
        isVisible={true}
        className="h-3/6 md:w-6/12 lg:w-7/12 flex flex-col justify-center items-center md:items-start mt-16 md:m-0"
      >
        <h1 className="text-2xl lg:text-4xl text-center md:text-left mb-5 mx-14 md:mx-0 md:w-9/12">{title}</h1>
        {subTitle && <p className="text-base lg:text-xl text-center md:text-left mb-5 mx-5 md:mx-0">{subTitle}</p>}
        {providerImageUrl && (
          <div className="avatar flex justify-center">
            <div className="mb-5 text-center h-10">
              <img src={providerImageUrl} />
            </div>
          </div>
        )}
        {description && (
          <div
            tabIndex={0}
            className="collapse w-96 border rounded-box border-base-300 collapse-arrow mb-5"
          >
            <div className="collapse-title text-xl font-medium">
              Descripción
            </div>
            <div className="collapse-content">
              <p>{description}</p>
            </div>
          </div>
        )}
        <div className="flex justify-center space-x-2">
          <button
            className="btn btn-primary btn-active"
            role="button"
            aria-pressed="true"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
              />
            </svg>
            Comprar
          </button>
          <button className="btn btn-outline btn-primary">Detalles</button>
        </div>
      </Animated>
    </SlideStyled>
  );
};

export default ProductSlider;
