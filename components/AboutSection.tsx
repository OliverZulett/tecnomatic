import React, { FunctionComponent } from "react";
import styled from "styled-components";

interface AboutSectionProps {
  
}

const AboutSectionStyled = styled.div`
  min-height: 400px;
`;
 
const AboutSection: FunctionComponent<AboutSectionProps> = () => {
  return (
    <AboutSectionStyled className="flex flex-col justify-center items-center">
      <div className="text-1xl md:text-2xl text-primary mb-3">conozca nuestra avanzada tecnologia</div>
      <div className="text-5xl md:text-6xl font-extrabold mb-3">TECNOMATIC</div>
      <p className="text-1xl md:text-2xl text-gray-400 mb-5">realizamos diseños de soluciones <br/> de acuerdo a sus requerimientos</p>
      <div>
        <button className="btn btn-primary">Conozca mas</button>
      </div>
    </AboutSectionStyled>
  );
}
 
export default AboutSection;
