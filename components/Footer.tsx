import React from 'react'

export default function Footer() {
  return (
    <footer className="p-10 footer bg-base-200 text-base-content">
      <div>
        <img width="300" height="300" src="https://tecnomatic.com.bo/wp-content/uploads/2020/04/LogoDefTecnomatic-1-scaled.jpg" className="fill-current">
        </img> 
        <p>TECNOMATIC dando las mejores soluciones desde 1992
        </p>
      </div> 
      <div>
        <span className="footer-title">Dirección</span> 
        <a className="link link-hover">Pasaje C de la Av. Thunupa #26</a> 
        <a className="link link-hover">Zona Sarcobamba</a> 
        <a className="link link-hover">Final América Oeste</a> 
      </div> 
      <div>
        <span className="footer-title">Compañia</span> 
        <a className="link link-hover">Acerca de nosotros</a> 
        <a className="link link-hover">Contacto</a> 
      </div> 
      <div>
        <span className="footer-title">Redes Sociales</span> 
        <a className="link link-hover">Facebook</a> 
        <a className="link link-hover">WhatsApp</a> 
        <a className="link link-hover">Instagram</a>
      </div>
    </footer>
  )
}
