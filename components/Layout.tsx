import React from "react";
import Footer from "./Footer";
import Navbar from "./Navbar";

type LayoutProps = {
  children: React.ReactElement
}

const Layout: React.FunctionComponent<LayoutProps> = (props: LayoutProps) => {
  return (
    <>
      <Navbar/>

      <main>
        {props.children}
      </main>
      
      <Footer/>
    </>
  );
}

export default Layout;