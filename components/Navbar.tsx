import React from "react";

export default function Navbar() {
  return (
    <div className="navbar mb-2 shadow-lg bg-primary-focus text-neutral-content">
      <div className="lg:px-2 lg:mx-2 navbar-start">
        <div className="hidden lg:flex items-stretch">
          <a className="btn btn-ghost btn-sm rounded-btn">Inicio</a>
          <a className="btn btn-ghost btn-sm rounded-btn">Contacto</a>
          <a className="btn btn-ghost btn-sm rounded-btn">Mi Cuenta</a>
          <a className="btn btn-ghost btn-sm rounded-btn">Conócenos</a>
        </div>

        <div className="dropdown">
          <button className="lg:hidden btn btn-square btn-ghost">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              className="inline-block w-6 h-6 stroke-current"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16M4 18h16"
              ></path>
            </svg>
          </button>
          <ul
            tabIndex={0}
            className="p-2 shadow menu dropdown-content bg-base-100 rounded-box w-52"
          >
            <li className="text-primary">
              <a>Inicio</a>
            </li>
            <li className="text-primary">
              <a>Contacto</a>
            </li>
            <li className="text-primary">
              <a>Mi Cuenta</a>
            </li>
            <li className="text-primary">
              <a>Conócenos</a>
            </li>
            <li className="text-primary">
              <div className="sm:hidden form-control">
                <div className="flex space-x-2">
                  <input
                    placeholder="Search"
                    className="w-full input input-primary input-bordered text-gray-600"
                    type="text"
                  />
                  <button className="btn btn-primary">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      className="inline-block w-6 h-6 stroke-current"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      ></path>
                    </svg>
                  </button>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <span className="lg:hidden ml-2 text-2xl font-bold">TECNOMATIC</span>
      </div>
      <div className="hidden px-2 mx-2 navbar-center lg:flex">
        <span className="text-3xl font-bold">
        <img width="250" height="250" src="https://tecnomatic.com.bo/wp-content/uploads/2020/04/LogoDefTecnomatic-1-scaled.jpg" className="fill-current">
        </img>
        </span>
      </div>
      <div className="navbar-end">
        <div className="hidden sm:flex">
          <div className="form-control">
            <div className="flex space-x-2">
              <input
                placeholder="Buscar"
                className="w-full input input-primary input-bordered text-gray-600"
                type="text"
              />
              <button className="btn btn-primary">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  className="inline-block w-6 h-6 stroke-current"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                  ></path>
                </svg>
              </button>
            </div>
          </div>
        </div>

        <div className="flex-none">
          <button className="btn btn-square btn-ghost">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
              />
            </svg>
          </button>
        </div>
      </div>
    </div>
  );
}
