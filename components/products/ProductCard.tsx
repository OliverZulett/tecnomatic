import React from "react";

export default function ProductCard() {
  return (
    <div className="w-11/12 md:w-1/2 lg:w-1/3  2xl:w-1/4 p-5 py-5 sm:px-20 md:p-5">
      <div className="card bordered shadow-lg">
        <figure>
          <img src="https://tecnomatic.com.bo/wp-content/uploads/2020/05/K40.png" />
        </figure>
        <div className="card-body">
          <h2 className="card-title">
            Sensor de Huella
            <div className="badge mx-2 badge-primary">Nuevo</div>
          </h2>
          <p>
            Rerum reiciendis beatae tenetur excepturi aut pariatur est eos. Sit
            sit necessitatibus veritatis sed molestiae voluptates incidunt iure
            sapiente.
          </p>
          <div className="justify-end card-actions">
            <button
              className="btn btn-sm text-sm btn-primary btn-active"
              role="button"
              aria-pressed="true"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                />
              </svg>
              Comprar
            </button>
            <button className="btn btn-sm text-sm btn-outline btn-primary">Detalles</button>
          </div>
        </div>
      </div>
    </div>
  );
}
