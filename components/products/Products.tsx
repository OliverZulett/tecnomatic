import React from 'react'
import MenuNavigation from './MenuNavigation'
import ProductGrid from './ProductGrid'

export default function Products() {
  return (
    <div className="container mx-auto">
      <div className="h-48 flex flex-wrap content-center justify-center px-5">
        <div className="inline-block text-2xl font-extrabold border-b-8 lg:text-5xl text-base-content border-primary">NUESTROS PRODUCTOS</div>
      </div>
      <div className="flex flex-col">
        <MenuNavigation/>
        <ProductGrid/>
      </div>
    </div>
  )
}
